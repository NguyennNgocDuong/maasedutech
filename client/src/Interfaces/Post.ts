export interface PostBody {
  title: string;
  content: string;
  image: string;
}
