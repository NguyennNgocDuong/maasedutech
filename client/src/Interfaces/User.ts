export interface UserBody {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}
export interface UserBodyLogin {
  email: string;
  password: string;
}

export interface VerifyOTPBody {
  email: string;
  otp: string;
}
