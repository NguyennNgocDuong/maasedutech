import { UserBody, UserBodyLogin, VerifyOTPBody } from "@/Interfaces/User";
import { https } from "./axiosClient";
import { PostBody } from "@/Interfaces/Post";

class HandlePost {
  postBlog = (data: FormData) => {
    const url = `/blog`;
    return https.post(url, data);
  };
  getBlogs = () => {
    const url = `/blog`;
    return https.get(url);
  };
  getDetailBlog = (id: string[] | string) => {
    const url = `/blog/current-blog/${id}`;
    return https.get(url);
  };
  updateBlog = (id: string[] | string, data: any) => {
    const url = `/blog/${id}`;
    return https.put(url, data);
  };
  deleteBlog = (id: string[] | string) => {
    const url = `/blog/${id}`;
    return https.delete(url);
  };
}

const handlePost = new HandlePost();

export default handlePost;
