import { UserBody, UserBodyLogin, VerifyOTPBody } from "@/Interfaces/User";
import { https } from "./axiosClient";

class HandleAuth {
  register = (data: UserBody) => {
    const url = "/user/register";
    return https.post(url, data);
  };
  verifyOTP = (data: VerifyOTPBody) => {
    const url = "/user/verify-email";
    return https.post(url, data);
  };
  login = (data: UserBodyLogin) => {
    const url = `/user/login`;
    return https.post(url, data);
  };
  getCurrentUser = () => {
    const url = `/user/current-user`;
    return https.get(url);
  };
}

const handleAuth = new HandleAuth();

export default handleAuth;
