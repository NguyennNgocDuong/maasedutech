"use client";

import { loginSchema, toastMsg } from "@/utils/constant";
import {
  Flex,
  Box,
  FormControl,
  FormLabel,
  Input,
  Checkbox,
  Stack,
  Button,
  Heading,
  Text,
  useColorModeValue,
  Container,
  FormErrorMessage,
  useToast,
  InputGroup,
  InputRightElement,
} from "@chakra-ui/react";
import { Form, Formik, Field } from "formik";
import { useDispatch } from "react-redux";
import { login } from "@/redux/slice/userSlice";
import { useRouter } from "next/navigation";
import { useState } from "react";
import { ViewIcon, ViewOffIcon } from "@chakra-ui/icons";
import handleAuth from "@/api/HandleAuth";
import { AppDispatch } from "@/redux/store";

export default function SignIn() {
  const dispatch = useDispatch<AppDispatch>();
  const toast = useToast();
  const router = useRouter();
  const [showPassword, setShowPassword] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  return (
    <Flex
      minH={"80vh"}
      align={"center"}
      justify={"center"}
      bg={useColorModeValue("gray.50", "gray.800")}
    >
      <Container maxW={"7xl"}>
        <Stack spacing={8} mx={"auto"} maxW={"lg"} py={12} px={6}>
          <Stack align={"center"}>
            <Heading fontSize={"4xl"}>Sign in to your account</Heading>
          </Stack>
          <Box
            rounded={"lg"}
            bg={useColorModeValue("white", "gray.700")}
            boxShadow={"lg"}
            p={8}
          >
            <Formik
              initialValues={{
                email: "",
                password: "",
              }}
              validationSchema={loginSchema}
              onSubmit={async (values, actions) => {
                setIsLoading(true);
                const { data } = await handleAuth.login(values);
                if (data.success) {
                  setIsLoading(false);

                  toastMsg(toast, data.msg, "success");
                  dispatch(
                    login({
                      isLoggedIn: true,
                      accessToken: data.accessToken,
                      currentUser: data.userData,
                    })
                  );

                  router.push("/");
                } else {
                  setIsLoading(false);

                  toastMsg(toast, data.msg, "error");
                }
              }}
            >
              {(props) => (
                <Form>
                  <Stack spacing={2}>
                    <Field name="email">
                      {({ field, form }: any) => (
                        <FormControl
                          isInvalid={form.errors.email && form.touched.email}
                          isRequired
                        >
                          <FormLabel>Email</FormLabel>
                          <Input {...field} />
                          <FormErrorMessage>
                            {form.errors.email}
                          </FormErrorMessage>
                        </FormControl>
                      )}
                    </Field>
                    <Field name="password">
                      {({ field, form }: any) => (
                        <FormControl
                          isInvalid={
                            form.errors.password && form.touched.password
                          }
                          isRequired
                        >
                          <FormLabel>Password</FormLabel>
                          <InputGroup>
                            <Input
                              {...field}
                              type={showPassword ? "text" : "password"}
                            />

                            <InputRightElement h={"full"}>
                              <Button
                                variant={"ghost"}
                                onClick={() =>
                                  setShowPassword(
                                    (showPassword) => !showPassword
                                  )
                                }
                              >
                                {showPassword ? <ViewIcon /> : <ViewOffIcon />}
                              </Button>
                            </InputRightElement>
                          </InputGroup>
                          <FormErrorMessage>
                            {form.errors.password}
                          </FormErrorMessage>
                        </FormControl>
                      )}
                    </Field>
                    <Stack spacing={10} pt={2}>
                      <Button
                        isLoading={isLoading}
                        loadingText="Submitting"
                        size="lg"
                        bg={"yellow.400"}
                        color={"white"}
                        _hover={{
                          bg: "yellow.500",
                        }}
                        type="submit"
                      >
                        Sign in
                      </Button>
                    </Stack>
                  </Stack>
                </Form>
              )}
            </Formik>
          </Box>
        </Stack>
      </Container>
    </Flex>
  );
}
