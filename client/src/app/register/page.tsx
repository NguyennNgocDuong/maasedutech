"use client";

import {
  Flex,
  Box,
  FormControl,
  FormLabel,
  Input,
  InputGroup,
  HStack,
  InputRightElement,
  Stack,
  Button,
  Heading,
  Text,
  useColorModeValue,
  FormErrorMessage,
  Container,
  useDisclosure,
  useToast,
} from "@chakra-ui/react";
import { useState } from "react";
import { ViewIcon, ViewOffIcon } from "@chakra-ui/icons";
import Link from "next/link";
import { Form, Formik, Field } from "formik";
import { toastMsg, userSchema } from "@/utils/constant";
import VerifyEmail from "@/components/Modal/VerifyEmail";
import handleAuth from "@/api/HandleAuth";

export default function Register() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [showPassword, setShowPassword] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const toast = useToast();

  return (
    <Flex
      minH={"30vh"}
      align={"center"}
      justify={"center"}
      bg={useColorModeValue("gray.50", "gray.800")}
    >
      <Container maxW={"7xl"}>
        <Stack spacing={8} mx={"auto"} maxW={"lg"} py={12} px={6}>
          <Stack align={"center"}>
            <Heading fontSize={"4xl"} textAlign={"center"}>
              Sign up
            </Heading>
          </Stack>
          <Box
            rounded={"lg"}
            bg={useColorModeValue("white", "gray.700")}
            boxShadow={"lg"}
            p={8}
          >
            <Formik
              initialValues={{
                firstName: "",
                lastName: "",
                email: "",
                password: "",
              }}
              validationSchema={userSchema}
              onSubmit={async (values, actions) => {
                setIsLoading(true);
                const response = await handleAuth.register(values);

                if (response.data.success) {
                  setIsLoading(false);

                  onOpen();
                } else {
                  setIsLoading(false);

                  toastMsg(toast, response.data?.msg, "error");
                }
              }}
            >
              {(props) => (
                <Form>
                  <VerifyEmail
                    onOpen={onOpen}
                    isOpen={isOpen}
                    onClose={onClose}
                    email={props.values.email}
                  />
                  <Stack spacing={2}>
                    <Field name="firstName">
                      {({ field, form }: any) => (
                        <FormControl
                          isInvalid={
                            form.errors.firstName && form.touched.firstName
                          }
                          isRequired
                        >
                          <FormLabel>First name</FormLabel>
                          <div>
                            <Input {...field} />
                            <FormErrorMessage>
                              {form.errors.firstName}
                            </FormErrorMessage>
                          </div>
                        </FormControl>
                      )}
                    </Field>

                    <Field name="lastName">
                      {({ field, form }: any) => (
                        <FormControl
                          isInvalid={
                            form.errors.lastName && form.touched.lastName
                          }
                          isRequired
                        >
                          <FormLabel>Last name</FormLabel>
                          <div>
                            <Input {...field} />
                            <FormErrorMessage>
                              {form.errors.lastName}
                            </FormErrorMessage>
                          </div>
                        </FormControl>
                      )}
                    </Field>

                    <Field name="email">
                      {({ field, form }: any) => (
                        <FormControl
                          isInvalid={form.errors.email && form.touched.email}
                          isRequired
                        >
                          <FormLabel>Email</FormLabel>
                          <Input {...field} />
                          <FormErrorMessage>
                            {form.errors.email}
                          </FormErrorMessage>
                        </FormControl>
                      )}
                    </Field>
                    <Field name="password">
                      {({ field, form }: any) => (
                        <FormControl
                          isInvalid={
                            form.errors.password && form.touched.password
                          }
                          isRequired
                        >
                          <FormLabel>Password</FormLabel>
                          <InputGroup>
                            <Input
                              {...field}
                              type={showPassword ? "text" : "password"}
                            />

                            <InputRightElement h={"full"}>
                              <Button
                                variant={"ghost"}
                                onClick={() =>
                                  setShowPassword(
                                    (showPassword) => !showPassword
                                  )
                                }
                              >
                                {showPassword ? <ViewIcon /> : <ViewOffIcon />}
                              </Button>
                            </InputRightElement>
                          </InputGroup>
                          <FormErrorMessage>
                            {form.errors.password}
                          </FormErrorMessage>
                        </FormControl>
                      )}
                    </Field>
                    <Stack spacing={10} pt={2}>
                      <Button
                        isLoading={isLoading}
                        loadingText="Submitting"
                        size="lg"
                        bg={"yellow.400"}
                        color={"white"}
                        _hover={{
                          bg: "yellow.500",
                        }}
                        type="submit"
                      >
                        Sign up
                      </Button>
                    </Stack>
                    <Stack pt={2}>
                      <Text align={"center"}>
                        Already a account?
                        <Link
                          href={"/login"}
                          style={{ color: "blue", marginLeft: "5px" }}
                        >
                          Login
                        </Link>
                      </Text>
                    </Stack>
                  </Stack>
                </Form>
              )}
            </Formik>
          </Box>
        </Stack>
      </Container>
    </Flex>
  );
}
