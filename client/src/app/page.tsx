"use client";

import Image from "next/image";
import styles from "./page.module.css";
import {
  Box,
  Container,
  Grid,
  GridItem,
  useDisclosure,
} from "@chakra-ui/react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@/redux/store";
import { useRouter } from "next/navigation";
import CreatePost from "@/components/Modal/CreatePost";
import ListPost from "@/components/ListPost";

export default function Home() {
  const { isLoggedIn, currentUser } = useSelector(
    (state: RootState) => state.userSlice
  );
  const { isOpen, onOpen, onClose } = useDisclosure();
  const router = useRouter();

  return (
    <Container maxW={"7xl"}>
      <CreatePost isOpen={isOpen} onClose={onClose} />
      <Grid templateColumns="repeat(3, 1fr)" gap={5} mt={5}>
        <GridItem>
          <Box
            onClick={() => onOpen()}
            bg={"#eee"}
            p={2}
            rounded={"xl"}
            cursor={"pointer"}
            color={"gray.400"}
          >
            {currentUser ? (
              <p>
                {currentUser?.firstName + currentUser?.lastName}, What's news
                today?
              </p>
            ) : (
              <p onClick={() => router.push(`/login`)}>
                Login to create your post!
              </p>
            )}
          </Box>
        </GridItem>
        <GridItem colSpan={2}>
          <ListPost />
        </GridItem>
      </Grid>
    </Container>
  );
}
