"use client";
import React, { useEffect, useState } from "react";
import { useParams } from "next/navigation";
import handlePost from "@/api/HandlePost";
import {
  Box,
  Container,
  Flex,
  Heading,
  Text,
  useDisclosure,
  useToast,
} from "@chakra-ui/react";
import { formatCreatedAt, toastMsg } from "@/utils/constant";
import Image from "next/image";
import { DeleteIcon, EditIcon } from "@chakra-ui/icons";
import UpdatePost from "@/components/Modal/UpdatePost";
import { useRouter } from "next/navigation";
import { useSelector } from "react-redux";
import { RootState } from "@/redux/store";
const DetailPost = () => {
  const { currentUser } = useSelector((state: RootState) => state.userSlice);
  const params = useParams();
  const [post, setPost] = useState<any>(null);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const toast = useToast();
  const router = useRouter();
  const fecthDetailPost = async () => {
    const { data } = await handlePost.getDetailBlog(params.id);
    if (data?.success) {
      setPost(data?.blog);
    }
  };

  const deletePost = async () => {
    const { data } = await handlePost.deleteBlog(params.id);
    if (data?.success) {
      toastMsg(toast, data?.msg, "success");
      router.push("/");
    }
  };

  useEffect(() => {
    fecthDetailPost();
  }, []);

  return (
    <Container maxW={"7xl"} mt={5}>
      <UpdatePost data={post} isOpen={isOpen} onClose={onClose} />
      <Box>
        <Flex justifyContent={"space-between"}>
          <Heading>{post?.title}</Heading>
          {currentUser?._id === post?.author?._id && (
            <Box>
              <EditIcon
                onClick={() => onOpen()}
                color={"green.300"}
                fontSize="2xl"
                cursor="pointer"
                mr={3}
              />
              <DeleteIcon
                onClick={deletePost}
                color={"red.300"}
                fontSize="2xl"
                cursor="pointer"
              />
            </Box>
          )}
        </Flex>
        <Flex color={"gray.500"}>
          <Text mr={1}>
            {post?.author?.firstName}
            {post?.author?.lastName} -
          </Text>
          <Text> {formatCreatedAt(post?.createdAt)}</Text>
        </Flex>
        <Box mt={5}>
          <Image alt="image-post" width="500" height="500" src={post?.image} />
          <Text mt={5}>{post?.content}</Text>
        </Box>
      </Box>
    </Container>
  );
};

export default DetailPost;
