"use client";

import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button,
  useToast,
} from "@chakra-ui/react";
import OtpInput from "react-otp-input";
import React, { memo, useCallback, useEffect, useState } from "react";
import handleAuth from "@/api/HandleAuth";
import { formatTime, toastMsg } from "@/utils/constant";
import { useRouter } from "next/navigation";

export default function VerifyEmail({ isOpen, onOpen, onClose, email }: any) {
  const [otp, setOtp] = useState("");
  const [remainingTime, setRemainingTime] = useState(60);
  const toast = useToast();
  const router = useRouter();

  const handleVerifyOTP = useCallback(async () => {
    if (!otp || otp.length < 6) {
      toastMsg(toast, "Please enter OTP", "error");

      await handleAuth.verifyOTP({
        email,
        otp,
      });
    } else {
      const response = await handleAuth.verifyOTP({
        email,
        otp,
      });

      console.log("response: ", response);

      if (response.data?.success) {
        onClose();
        toastMsg(toast, response.data.msg, "success");
        router.push("/login");
      } else {
        onClose();
        toastMsg(toast, response.data.msg, "error");
      }
    }
  }, [otp]);

  useEffect(() => {
    if (isOpen) {
      const timer = setTimeout(() => {
        handleVerifyOTP();
        onClose();
      }, 60000);

      const interval = setInterval(() => {
        setRemainingTime((prevTime) => prevTime - 1);
      }, 1000); // Update remaining time every second

      return () => {
        clearTimeout(timer);
        clearInterval(interval);
        setOtp("");
        setRemainingTime(60);
      };
    }
  }, [isOpen]);
  return (
    <>
      <Modal
        closeOnOverlayClick={false}
        isOpen={isOpen}
        onClose={onClose}
        isCentered
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Verify Email</ModalHeader>
          <ModalBody pb={6}>
            <p className=" text-lg">
              Please check email <b className=" font-semibold">{email} </b>
              and enter your OTP.
            </p>
            <p className=" text-lg">
              Time remaining <i>{formatTime(remainingTime)}</i>
            </p>
            <OtpInput
              containerStyle={{ justifyContent: "between", marginTop: "5px" }}
              value={otp}
              onChange={setOtp}
              numInputs={6}
              renderSeparator={<span>- </span>}
              renderInput={(props) => <input {...props} />}
              // inputStyle="border-2 border- w-[50px] h-[50px] border-blue-500 outline-blue-500 text-lg"
              inputStyle={{
                width: "50px",
                height: "50px",
                borderWidth: "3px",
                // borderColor: "yellow",
                outlineColor: "yellow",
                marginRight: "5px",
                marginLeft: "5px",
              }}
            />
            <Button
              loadingText="Sending..."
              width={"100%"}
              bg={"yellow.400"}
              color={"white"}
              mt={5}
              _hover={{
                bg: "yellow.500",
              }}
              onClick={handleVerifyOTP}
            >
              Send
            </Button>
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
}
