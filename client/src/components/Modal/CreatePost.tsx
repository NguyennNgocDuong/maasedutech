"use client";

import handlePost from "@/api/HandlePost";
import { createPostSchema, toastMsg } from "@/utils/constant";
import { ArrowUpIcon } from "@chakra-ui/icons";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
  Button,
  Stack,
  FormControl,
  FormLabel,
  Input,
  FormErrorMessage,
  Textarea,
  Text,
  useToast,
} from "@chakra-ui/react";
import { Form, Formik, Field } from "formik";
import { FormEvent, useState } from "react";

export default function CreatePost({ isOpen, onOpen, onClose }: any) {
  const [image, setImage] = useState<any>("");
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const toast = useToast();
  return (
    <>
      <Modal isOpen={isOpen} onClose={onClose} size={"2xl"} isCentered>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Create Post</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Formik
              initialValues={{
                title: "",
                content: "",
              }}
              validationSchema={createPostSchema}
              onSubmit={async (values, actions) => {
                setIsLoading(true);
                const formData = new FormData();

                for (let i of Object.entries({ ...values, image })) {
                  formData.append(i[0], i[1]);
                }
                const rs = await handlePost.postBlog(formData);
                if (rs?.data?.success) {
                  setIsLoading(false);
                  onClose();
                  toastMsg(toast, rs?.data?.msg, "success");
                } else {
                  setIsLoading(false);
                  onClose();
                  toastMsg(toast, rs?.data?.msg, "error");
                }
              }}
            >
              {(props) => (
                <Form>
                  <Stack spacing={2}>
                    <Field name="title">
                      {({ field, form }: any) => (
                        <FormControl
                          isInvalid={form.errors.title && form.touched.title}
                          isRequired
                        >
                          <FormLabel>Title</FormLabel>
                          <div>
                            <Input {...field} />
                            <FormErrorMessage>
                              {form.errors.title}
                            </FormErrorMessage>
                          </div>
                        </FormControl>
                      )}
                    </Field>

                    <Field name="content">
                      {({ field, form }: any) => (
                        <FormControl
                          isInvalid={
                            form.errors.content && form.touched.content
                          }
                          isRequired
                        >
                          <FormLabel>Content</FormLabel>
                          <div>
                            <Textarea {...field} />
                            <FormErrorMessage>
                              {form.errors.content}
                            </FormErrorMessage>
                          </div>
                        </FormControl>
                      )}
                    </Field>
                    <Field name="image">
                      {({ field, form }: any) => (
                        <FormControl
                          isInvalid={form.errors.image && form.touched.image}
                          isRequired
                        >
                          {/* <label
                            style={{
                              cursor: "pointer",
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                            htmlFor="image"
                          >
                            <ArrowUpIcon />
                            <Text>Upload Image</Text>
                          </label> */}
                          <input
                            onChange={(e: FormEvent<HTMLInputElement>) => {
                              const target = e.target as HTMLInputElement & {
                                files: FileList;
                              };
                              setImage(target.files[0]);
                            }}
                            type="file"
                            // {...field}
                          />
                        </FormControl>
                      )}
                    </Field>
                    <Stack spacing={10} pt={2}>
                      <Button
                        isLoading={isLoading}
                        loadingText="Submitting"
                        size="lg"
                        bg={"yellow.400"}
                        color={"white"}
                        _hover={{
                          bg: "yellow.500",
                        }}
                        type="submit"
                      >
                        Create
                      </Button>
                    </Stack>
                  </Stack>
                </Form>
              )}
            </Formik>
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
}
