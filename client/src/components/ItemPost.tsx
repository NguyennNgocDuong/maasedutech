"use client";

import Image from "next/image";
import {
  Box,
  Center,
  Heading,
  Text,
  Stack,
  Avatar,
  useColorModeValue,
} from "@chakra-ui/react";
import { formatCreatedAt } from "@/utils/constant";
import { useRouter } from "next/navigation";
import Link from "next/link";

export default function ItemPost({ post }: any) {
  const router = useRouter();

  return (
    <Center>
      <Box
        maxW={"380px"}
        cursor={"pointer"}
        w={"full"}
        // eslint-disable-next-line react-hooks/rules-of-hooks
        bg={useColorModeValue("white", "gray.900")}
        boxShadow={"2xl"}
        rounded={"md"}
        p={6}
        overflow={"hidden"}
      >
        <Link href={`/blog/${post._id}`}>
          <Box
            h={"210px"}
            bg={"gray.100"}
            mt={-6}
            mx={-6}
            mb={6}
            pos={"relative"}
          >
            <Image src={post.image} fill alt="Example" />
          </Box>
          <Stack>
            <Heading
              // eslint-disable-next-line react-hooks/rules-of-hooks
              color={useColorModeValue("gray.700", "white")}
              fontSize={"2xl"}
              fontFamily={"body"}
            >
              {post.title}
            </Heading>
          </Stack>
          <Stack mt={6} direction={"row"} spacing={4} align={"center"}>
            <Avatar
              src={"https://avatars0.githubusercontent.com/u/1164541?v=4"}
            />
            <Stack direction={"column"} spacing={0} fontSize={"sm"}>
              <Text fontWeight={600}>
                {post.author.firstName}
                {post.author.lastName}
              </Text>
              <Text color={"gray.500"}>{formatCreatedAt(post.createdAt)}</Text>
            </Stack>
          </Stack>
        </Link>
      </Box>
    </Center>
  );
}
