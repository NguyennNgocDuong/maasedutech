"use client";

import handlePost from "@/api/HandlePost";
import React, { useEffect, useState } from "react";
import ItemPost from "./ItemPost";
import { Grid, GridItem } from "@chakra-ui/react";

const ListPost = () => {
  const [posts, setPosts] = useState<any[]>([]);
  console.log("posts: ", posts);

  const fetchPost = async () => {
    const response = await handlePost.getBlogs();
    if (response?.data?.success) {
      setPosts(response?.data?.posts);
    }
  };

  useEffect(() => {
    fetchPost();
  }, []);

  return (
    <Grid templateColumns="repeat(2, 1fr)" gap={5}>
      {posts.map((post): any => {
        return (
          <GridItem>
            <ItemPost post={post} />
          </GridItem>
        );
      })}
    </Grid>
  );
};

export default ListPost;
