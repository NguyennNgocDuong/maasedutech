import moment from "moment";
import * as Yup from "yup";

export const userSchema = Yup.object({
  firstName: Yup.string()
    .max(6, "Your first name must be under 6 characters!")
    .required("You must fill in this section!"),
  lastName: Yup.string()

    .max(6, "Your last name must be under 6 characters!")
    .required("You must fill in this section!"),

  email: Yup.string()
    .email("Invalid Email")
    .required("You must fill in this section!"),
  password: Yup.string()
    .min(6, "Your password must be at least 6 characters!")
    .required("You must fill in this section!"),
});

export const loginSchema = Yup.object({
  email: Yup.string()
    .email("Invalid Email")
    .required("You must fill in this section!"),
  password: Yup.string()
    .min(6, "Your password must be at least 6 characters!")
    .required("You must fill in this section!"),
});
export const createPostSchema = Yup.object({
  title: Yup.string().required("You must fill in this section!"),
  content: Yup.string().required("You must fill in this section!"),
});

export const toastMsg = (toast: any, description: string, status: string) => {
  return toast({
    position: "top-right",

    description: description,
    status: status,
    duration: 2000,
    isClosable: true,
  });
};

export const formatTime = (seconds: number) => {
  const mins = Math.floor(seconds / 60);
  const secs = seconds % 60;
  return `${mins.toString().padStart(2, "0")}:${secs
    .toString()
    .padStart(2, "0")}`;
};

export const formatCreatedAt = (time: any) => {
  return moment(time).format("DD/MM/YYYY ");
};
