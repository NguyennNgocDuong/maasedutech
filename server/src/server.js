import express from "express";
import dbConnect from './config/dbConnect'
import initRoutes from "./routes/api"
import cookieParser from "cookie-parser"
import cors from "cors"
import path from "path";

require("dotenv").config()

const app = express();
const port = process.env.PORT || 8888

app.use(cors({ origin: process.env.CLIENT_URL, methods: ['POST', 'GET', 'DELETE', 'PUT'], credentials: true }))


app.use(cookieParser())
app.use(express.json())
app.use(express.static(path.join(__dirname, 'public')))

app.use(express.urlencoded({ extended: true }));
dbConnect()

initRoutes(app)

const server = app.listen(port, () => {
    console.log("server runningg.....");
})

