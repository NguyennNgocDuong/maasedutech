import Blog from "../models/blog"
import asyncHandler from "express-async-handler"

const createBlog = asyncHandler(async (req, res) => {
    const { _id } = req.user
    const { path } = req.file


    const newBlog = await Blog.create({ ...req.body, author: _id, image: path })

    return res.status(200).json({
        success: newBlog ? true : false,
        msg: newBlog ? "Create post successfully!" : "Failed!"
    })



})
const getAllBlogs = asyncHandler(async (req, res) => {


    const posts = await Blog.find().populate("author", "firstName lastName")

    return res.status(200).json({
        success: posts ? true : false,
        posts

    })

})
const updateBlog = asyncHandler(async (req, res) => {

    const { content, title } = req.body

    const { id } = req.params
    const { _id } = req.user

    const blog = await Blog.findById(id)

    if (blog?.author?.toString() === _id) {
        const rs = await Blog.findByIdAndUpdate(id, { title, content, image: req.file?.path, }, { new: true })

        return res.status(200).json({
            success: rs ? true : false,
            msg: rs ? "Update your news succesful!" : "Failed!"
        })
    } else {
        return res.status(203).json({
            success: false,
            msg: "Non-Authoritative Information"
        })
    }



})
const deleteBlog = asyncHandler(async (req, res) => {
    const { id } = req.params
    const { _id } = req.user
    const blog = await Blog.findById(id)



    if (blog?.author?.toString() === _id) {
        const rs = await Blog.findByIdAndDelete(id)
        return res.status(200).json({
            success: rs ? true : false,
            msg: rs ? "Remove your news succesful!" : "Failed!"
        })
    } else {
        return res.status(203).json({
            success: false,
            msg: "Non-Authoritative Information"
        })
    }




})

/**
 * Khi người dùng like một bài blog:
 * 1. Check xem người dùng đã dislike hay chưa => nếu có thì bỏ dislike
 * 2. Check xem người dùng đã like trước đó chưa => nếu có thì bỏ like, không thì like
 */

const likeBlog = asyncHandler(async (req, res) => {
    const { _id } = req.user
    const { id } = req.params
    const blog = await Blog.findById(id)
    if (blog) {


        // Check xem người dùng đã like trước đó chưa => nếu có thì bỏ like, không thì like
        const alreadyLiked = blog?.likes.find(el => el.toString() === _id)
        if (alreadyLiked) {
            await Blog.findByIdAndUpdate(id, { $pull: { likes: _id } }, { new: true })
        } else {
            await Blog.findByIdAndUpdate(id, { $push: { likes: _id } }, { new: true })

        }
    }
    return res.status(200).json({
        success: blog ? true : false
    })

})
const disLikeBlog = asyncHandler(async (req, res) => {
    // const { _id } = req.user
    // const { id } = req.params
    // const blog = await Blog.findById(id)
    // if (blog) {
    //     //  * 1. Check xem người dùng đã like hay chưa => nếu có thì bỏ like
    //     const alreadyLiked = blog?.likes.find(el => el.toString() === _id)
    //     if (alreadyLiked) {
    //         await Blog.findByIdAndUpdate(id, { $pull: { likes: _id } }, { new: true })
    //     }

    //     //  * 2. Check xem người dùng đã like trước đó chưa => nếu có thì bỏ like, không thì like
    //     const alreadyDisLiked = blog?.disLikes.find(el => el.toString() === _id)
    //     if (alreadyDisLiked) {
    //         await Blog.findByIdAndUpdate(id, { $pull: { disLikes: _id } }, { new: true })
    //     } else {
    //         await Blog.findByIdAndUpdate(id, { $push: { disLikes: _id } }, { new: true })

    //     }



    // }
    // return res.status(200).json({
    //     success: blog ? true : false
    // })

})

const selectFields = "firstName lastName"
const getCurrentBlog = asyncHandler(async (req, res) => {
    const { id } = req.params
    const blog = await Blog.findByIdAndUpdate(id, { $inc: { views: 1 } }, { new: true }).populate("author", "firstName lastName image")
        .populate("likes", selectFields)
    return res.status(200).json({
        success: blog ? true : false,
        blog
    })
})


const uploadImageBlog = asyncHandler(async (req, res) => {
    const { id } = req.params
    const blog = await Blog.findByIdAndUpdate(id, { image: req.file.path }, { new: true })

    return res.status(200).json({
        success: blog ? true : false,
        msg: blog ? "Upload image successfully!" : "Cannot upload image!"

    })

})

module.exports = { createBlog, getAllBlogs, updateBlog, deleteBlog, getCurrentBlog, likeBlog, disLikeBlog, uploadImageBlog }